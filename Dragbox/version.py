# Copyright (C) 2006 Ulrik Sverdrup
#
#   dragbox -- Commandline drag-and-drop tool for GNOME
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

# Global constants file
# Dragbox/version.py.  Generated from version.py.in by configure.

package_name ="dragbox"
prefix = "/usr/local"
data_dir = "/usr/local/share"
glade_directory = "/usr/local/share/dragbox"
version = "0.4.0"
pref_id = "/apps/dragbox/"
debug_enabled = "no"
git_version = "bbb96c9ce3e61417b8bdd58913a88b2eef6cfab5"
git_date = "Fri, 25 Apr 2008 14:21:04 +0200"

copyright_info = """(C) 2006--2008, Ulrik Sverdrup
dragbox is free software; it is distributed under
the terms of the GNU General Public License."""
