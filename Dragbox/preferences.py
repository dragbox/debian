# Copyright (C) 2006 Ulrik Sverdrup
#
#   dragbox -- Commandline drag-and-drop tool for GNOME
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

u"""
Preference database handlers
interfaces with gconf
"""
from version import pref_id

from gconf import client_get_default
shared_client = client_get_default()

def get_gconf_client():
	"""
	Returns a gconf client object
	"""
	return shared_client
	
def get_bool(key, if_none=None):
	""" 
	get a bool value
	
	@param key: key in the application domain to get 
	@type key: a string object. 
	
	@param if_none: Placeholder if no value is set 
	@type if_none: a bool object.    
	""" 
	client = shared_client
	gcvalue = client.get_without_default(pref_id + key)
	 
 	if gcvalue:
 		value = gcvalue.get_bool()
 	else: 
 		value = if_none
 	 
 	return value

def set_bool(key, value):
	""" 
	Sets a key in the application domain
	
	@param key: key 
	@type key: a string object. 
	
	@param value:   
	@type value: a bool object.    
	""" 
	client = shared_client
	client.set_bool(pref_id + key, value)

def notify_add(key, callback, user_info=None):
	""" 
	Registers a delegate for callbacks when a key is changed
	
	Callback id:
	callback(client, connection, entry, user_info):
	
	@param key: The prefs key that changes 
	@type key: a string object. 

	@param callback: 
	@type callback: a function object.
	
	@param user_info: optional user info 
	@type user_info: an object.    
	""" 
	from utils import print_debug 
	#print_debug("notify-add: %s" % (dragbox_pref_id + key,))
	shared_client.notify_add(pref_id + key, callback, user_info)
	  