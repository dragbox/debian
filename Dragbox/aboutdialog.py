license_text = """
  dragbox -- Command line drag-and-drop tool for Gnome

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
USA
"""
authors = ["Ulrik Sverdrup <ulrik.sverdrup@gmail.com>"]

import version
from gtk import AboutDialog

_about_dialog = None

def show_about_dialog(*ignored, **kwds):
	"""
	create an about dialog and show it
	"""
	# Use only one instance, stored in _about_dialog
	global _about_dialog
	if _about_dialog:
		ab = _about_dialog
	else:
		ab = AboutDialog()
		ab.set_program_name(version.package_name)
		ab.set_version(version.version)
		ab.set_copyright(version.copyright_info)
		ab.set_license(license_text)
		ab.set_authors(authors)
		ab.connect("response", _response_callback)
		# do not delete window on close
		ab.connect("delete-event", lambda *ign: True)
		_about_dialog = ab
	ab.present()

def _response_callback(dialog, response_id):
	dialog.hide()
