# Copyright (C) 2006, 2007 Ulrik Sverdrup
#
#   dragbox -- Commandline drag-and-drop tool for GNOME
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import gobject

from shelfitem import (make_item, TEXT_TYPE, FILE_TYPE,
		TARGET_TYPE_TEXT, TARGET_TYPE_URI_LIST)
import preferences

class Store (object):
	"""
	Data model for the collection of drag items
	"""
	def __init__(self):
		self.items = []

	def __contains__(self, item):
		return (item in self.items)
	
	def __iter__(self):
		return iter(self.items)
	
	def __len__(self):
		return len(self.items)

	def append(self, item):
		self.items.append(item)
	
	def remove(self, item):
		self.items.remove(item)

class DataController (gobject.GObject):
	"""
	Signals:
	item-added: parameters: added Item
	item-removed: parameters: removed Item
	"""
	__gtype_name__ = "DataController"
	def __init__(self):
		super(DataController, self).__init__()
		self.store = Store()
	
	def handle_drop_data(self, info, selection_data):
		"""
		Handle incoming drop data
		
		return True if data was handled
		"""
		if info == TARGET_TYPE_TEXT:
			return self._get_text_from_selection(selection_data)
		elif info == TARGET_TYPE_URI_LIST:
			return self._get_files_from_selection(selection_data)
		return False

	def _get_text_from_selection(self, selection_data):
		"""
		internal method to handle dropped text
		
		@param selection_data: Contains the dropped data
		@type selection_data: a gtk.SelectionData object.
		"""
		text = selection_data.get_text()
		if text:
			self._handle_new_item(text, TEXT_TYPE)
			return True

	def _get_files_from_selection(self, selection_data):
		"""
		internal method to handle dropped files
		
		@param selection_data: Contains the dropped data
		@type selection_data: a gtk.SelectionData object.
		"""
		files = selection_data.get_uris()
		from gnomevfs import get_local_path_from_uri
		
		for f in files:
			try:
				data = get_local_path_from_uri(f)
				data_type = FILE_TYPE
			except:
				data = f
				data_type = TEXT_TYPE
			if data:
				self._handle_new_item(data, data_type)
		return True

	def _handle_new_item(self, data, type):
		"""
		Add a new item to the store

		Print it to stdout if needed
		"""
		item = make_item(data, type)
		self.store.append(item)
		self.emit("item-added", item)
	
	def add_item(self, data, type):
		self._handle_new_item(data, type)
	
	def remove_item(self, item):
		self.store.remove(item)
		self.emit("item-removed", item)
	
	def check_if_valid(self, item):
		"""
		Check if item is valid or remove
		"""
		if not item.is_valid() and item in self.store:
			self.remove_item(item)
	
	def get_items(self):
		return iter(self.store)

	def activate(self, item):
		"""
		Activate one item
		"""
		if item.get_type() == FILE_TYPE:
			open = preferences.get_bool("open", if_none=False)
			if open:
				self._open_file(item)
				return
		self.put_on_clipboard(item)

	def _open_file(self, item):
		"""
		opens the file of the dragitem argument
		"""
		from gnome import url_show
		file_uri = item.get_uri()
		try:
			url_show(file_uri)
		except:
			print_error("Could not open %s" % file_uri)
	
	def put_on_clipboard(self, item):
		"""
		copies item to clipboard
		"""
		from gtk import clipboard_get
		# get default clipboard
		clip = clipboard_get()
		owner = clip.get_owner()
		if owner is not None:
			return
		
		drag_targets = item.get_targets()
		done_ignored = lambda *x: None
		clip.set_with_data(drag_targets, self._clipboard_copy,
				done_ignored, user_data=item)
		clip.set_can_store(drag_targets)
		clip.store() # try to force immediate retrieval

	def _clipboard_copy(self, clipboard, selection_data, target_type, item):
		"""
		callback called when clipboard copy data is retrieved
		"""
		if item:
			item.write_to_selection(selection_data, target_type)
		
		return True

gobject.type_register(DataController)
gobject.signal_new("item-added", DataController, gobject.SIGNAL_RUN_LAST,
		gobject.TYPE_BOOLEAN, (gobject.TYPE_PYOBJECT, ))
gobject.signal_new("item-removed", DataController, gobject.SIGNAL_RUN_LAST,
		gobject.TYPE_BOOLEAN, (gobject.TYPE_PYOBJECT, ))
